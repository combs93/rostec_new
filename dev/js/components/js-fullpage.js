$(document).ready(function() {
    var flag = false;
    var windowHeight;
    var sidebarSliderPosition;
    var slidesCount;
    var scrollingLineHeight;
    var scrollStepCount;

    function sidebarSliderScroll() {
        sidebarSliderPosition  = parseInt($('.sidebar-slider').css('top'));
        slidesCount = $('.js-fullpage .section').length;
        windowHeight = $(window).outerHeight();
        scrollingLineHeight = windowHeight - sidebarSliderPosition * 2;
        scrollStepCount = scrollingLineHeight / slidesCount;

        $('.sidebar-slider').css('height', scrollStepCount);
    }
    sidebarSliderScroll();
    $(window).on('resize', sidebarSliderScroll);
    
    function initFullpage() {
        if ($(window).outerWidth() > 991 && !flag) {
            new fullpage('#content_inner', {
                onLeave: function(origin, destination, direction){
                    
                    if (direction == 'up') {
                        $('.sidebar-slider').css('top', parseInt($('.sidebar-slider').css('top')) - scrollStepCount + 'px')
                    } else if (direction == 'down') {
                        $('.sidebar-slider').css('top', parseInt($('.sidebar-slider').css('top')) + scrollStepCount + 'px')
                    }
                }
            });
            flag = true;
        } else if ($(window).outerWidth() < 992) {
            if ($('.fp-enabled').length) {
                fullpage_api.destroy();
                flag = false;
            }
        }
    }
    
    initFullpage();
    $(window).on('resize', initFullpage);
});
