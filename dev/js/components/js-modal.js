$(document).ready(function() {
   var activeTimeout,
       modalCloseTimeout,
       modalTime = 10000;

   function initModal() {
      $('#modalWindow').modal({show: true, backdrop: 'static', keyboard: false}); 
      activeTimeout = setTimeout(function() {
         $('.timing-slider').animate({
            width: '100%',
         }, modalTime);
      }, 300);
      modalCloseTimeout = setTimeout(function() {
         $('#modalWindow').modal('hide');
         clearTimeout(modalCloseTimeout);
      },modalTime);
   }
   
   var test_flag = localStorage.getItem("modal_flag");

   if (!test_flag) {

      initModal();

      localStorage.setItem("modal_flag", true);
   
   }
   
   $('.popup-close').on('click', function() {
      $('#modalWindow').modal('hide');
      $('.timing-slider').css('width', '0');
      clearTimeout(modalCloseTimeout);
      clearTimeout(activeTimeout);
      $('.timing-slider').stop();
   });
   
   $('.modal-trigger-item').on('click', function() {
      $('.timing-slider').css('width', '0');
      initModal();
   });
});