/* frameworks */
//=include ../../dist/bower_components/jquery/dist/jquery.js
//=include ../../dist/bower_components/moff/dist/moff.min.js

/* libs */
//=include lib/modernizr-custom.js

/* plugins */
//=include ../../dist/bower_components/bootstrap/js/dist/util.js
//=include ../../dist/bower_components/bootstrap/js/dist/modal.js

/* separate */
//=include helpers/object-fit.js
//=include separate/global.js

/* components */
//=include components/js-preloader.js

// the main code

window.moffConfig = {
	// Website CSS breakpoints.
	// Default values from Twitter Bootstrap.
	// No need to set xs, because of Mobile first approach.
	breakpoints: {
		xs: 480,
		sm: 768,
		md: 992,
		lg: 1200
	},
	loadOnHover: true,
	cacheLiveTime: 2000
};

Moff.amd.register({
	id: 'modal',
	file: {
		js: ['s/js/components/js-modal.js']
	},

	beforeInclude: function() {},
    afterInclude: function() {},
    
	loadOnScreen: ['xs', 'sm', 'md', 'lg'],
	onWindowLoad: true
});

if($('.js-slick-init').length) {
	Moff.amd.register({
		id: 'slickSlider',
		depend: {
			js: ['bower_components/slick-carousel/slick/slick.min.js']
		},
		file: {
			js: ['s/js/components/js-slick.js']
		},
		
		beforeInclude: function() {},
		afterInclude: function() {},
		
		loadOnScreen: ['xs', 'sm', 'md', 'lg'],
		onWindowLoad: true
	});
}

if($('.js-fancybox').length) {
	Moff.amd.register({
		id: 'fancybox',
		depend: {
			js: ['bower_components/fancybox/dist/jquery.fancybox.min.js'],
			css: ['bower_components/fancybox/dist/jquery.fancybox.min.css']
		},
		file: {
			js: ['s/js/components/js-fancybox.js']
		},

		beforeInclude: function() {},
		afterInclude: function() {},
			
		loadOnScreen: ['xs', 'sm', 'md', 'lg'],
		onWindowLoad: true
	});
}

if($('.js-fullpage').length) {
	Moff.amd.register({
		id: 'fullpage',
		depend: {
			js: ['bower_components/fullpage.js/dist/fullpage.js']
		},
		file: {
			js: ['s/js/components/js-fullpage.js']
		},

		beforeInclude: function() {},
		afterInclude: function() {},
			
		loadOnScreen: ['xs', 'sm', 'md', 'lg'],
		onWindowLoad: true
	});
}

function onScroll() {
	if ($(window).scrollTop() > 10) {
		console.log('1');
		$('.person-header').addClass('sticky');
	} else {
		$('.person-header').removeClass('sticky');
	}
}

$(document).on('scroll', onScroll);

function getpresonHeaderHeight() {
	var presonHeaderHeight = $('.person-header').outerHeight();
	$('.person-header-wrapper').css('height', presonHeaderHeight)
} 
getpresonHeaderHeight();
$(window).on('resize', getpresonHeaderHeight);